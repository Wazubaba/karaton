# K A R A T O N
## Planning docs
Right now, the end goal for karaton is to serve as the engine for GMB. It will
leverage lua, libconfig, sfml, some sort of json lib probably, and most likely
a few other libs I've already managed to forget. Oh and spdlog.

All lua-interop-related things files should be prefixed with `kl`, for
`karaton lua`. All engine-related things should be prefixed simply with a `k`,
for `karaton`.

All the cpp side of karaton should do is handle preparing the lua environment,
initializing graphics, and basically serving as a big ass lua library. The
vast, overwhelming majority of the code will most likely be in lua, with any
performance-critical systems done in cpp instead.

## TODOs
consider swapping spdlog for a more tailored solution leveraging [this][1]
look into [this][2] ECS library.

[1]: https://fmt.dev/latest/index.html
[2]: https://www.reddit.com/r/cpp/comments/cx21wv/entt_v31_is_out_gaming_meets_modern_c_a_fast_and/


## Data formats
Lua is intended to serve as the driver. Libconfig data will be used as the
data declaration side of things (optionally, of course).

## Important notes
Don't get too far ahead of yourself. We need to simply recreate the old
combat mode from GMB first. Don't even think of other games for right now
until you've managed to actually get that far along first. Planning ahead is
fine, but don't get autistic or go overboard over it...

# Deployment
Let's try to generate an appimage for linux. Fuck the namefag autism.
That or docker idk lmfao
