#import os
import strformat
import ospaths

const PANDOC = findExe("pandoc")

if PANDOC == "":
  echo "Failed to find pandoc. Build cannot continue"
  quit(2)

let TEMPLATEFILE = thisDir() / "src/template.rst"


proc docgen(tgt: string) =
  for file in listFiles(getCurrentDir()):
    let fname = file.extractFilename().changeFileExt(".html")
    exec(&"{PANDOC} {file} {TEMPLATEFILE} -o {tgt}/{fname}")


task clean, "Clean docs":
  rmDir("generated")

task build, "Generate documentation":
  let tgt = thisDir() / "generated"

  echo "Preparing documentation environment .."
  mkdir(tgt)
  mkdir(tgt / "luaAPI")
  mkdir(tgt / "engine")


  withDir("src"):
    echo "Copying images .."
    cpDir("icons", tgt / "icons")
    exec(&"{PANDOC} index.rst -o {tgt}/index.html")
    withDir("luaAPI"):
      echo "Building documentation for the lua API"
      docgen(tgt / "luaAPI")

