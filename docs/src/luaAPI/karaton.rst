====================================
 Karaton - Core Namespace (karaton)
====================================
:Author: Wazubaba
:Version: 0.1.0

.. include:: ../template.rst


Introduction
============

This is the main namespace of the api. From here the various components are
stored in their own namespaces, but there might wind up being a few functions
and things in this one as well.

-------------------------

Modules
=======

* |moduleico| Console_ - Stuff for working with console input and output, mostly for
  debugging.



.. _Console: console.html


-------------------------

Functions
=========

None yet...

-------------------------

Types
=====

None yet...

