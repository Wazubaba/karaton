=========================================
 Karaton - Console API (karaton.console)
=========================================
:Author: Wazubaba
:Version: 0.1.0

Introduction
============

This namespace contains functions for interacting with a console. It probably
won't be that useful on windows, but is for things like logging and what-not,
mainly for debugging purposes.

-------------------------

Submodules
==========

None yet...

-------------------------

Functions
=========

* |functionico| karaton.console.message(string) - Displays a message to the console.
* |functionico| karaton.console.warn(string) - Displays a warning message to the console.
* |functionico| karaton.console.error(string) - Displays an error message to the console.

-------------------------

Types
=====

None yet...


