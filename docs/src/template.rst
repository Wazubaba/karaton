.. |moduleico| image:: ../icons/icon-module.png
  :height: 16px
  :width: 16px
  :scale: 100 %
  :alt: MODULE

.. |classico| image:: ../icons/icon-class.png
  :height: 16px
  :width: 16px
  :scale: 100 %
  :alt: MODULE

.. |functionico| image:: ../icons/icon-function.png
  :height: 16px
  :width: 16px
  :scale: 100 %
  :alt: MODULE

.. |typeico| image:: ../icons/icon-type.png
  :height: 16px
  :width: 16px
  :scale: 100 %
  :alt: MODULE

