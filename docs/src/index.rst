====================================
 Karaton Game Engine - Index
====================================
:Author: Wazubaba
:Version: 0.1.0

Introduction
============

This is the documentation for the various components of the Karaton Game Engine.

Karaton is divided into three separate components in this order:

1. `The Lua API`_
2. The data format
3. The C++ Engine Reference

.. _`The Lua API`: luaAPI/karaton.html
