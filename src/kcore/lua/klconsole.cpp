#include "kcore/lua/klconsole.hpp"

#include <spdlog/spdlog.h>

int l_debug(lua_State* L)
{
	if (!lua_isstring(L, -1))
	{
		const char* tstr = lua_typename(L, -1);
		spdlog::warn("LUA: Expected string for message, got {} instead", tstr);
	}
	else
	{
		const char* str = lua_tostring(L, -1);
		spdlog::debug("{}", str);
	}
	return 0;
}

int l_message(lua_State* L)
{
	if (!lua_isstring(L, -1))
	{
		const char* tstr = lua_typename(L, -1);
		spdlog::warn("LUA: Expected string for message, got {} instead", tstr);
	}
	else
	{
		const char* str = lua_tostring(L, -1);
		spdlog::info("{}", str);
	}
	return 0;
}

int l_warn(lua_State* L)
{
	if (!lua_isstring(L, -1))
	{
		const char* tstr = lua_typename(L, -1);
		spdlog::warn("LUA: Expected string for message, got {} instead", tstr);
	}
	else
	{
		const char* str = lua_tostring(L, -1);
		spdlog::warn("{}", str);
	}
	return 0;
}

int l_error(lua_State* L)
{
	if (!lua_isstring(L, -1))
	{
		const char* tstr = lua_typename(L, -1);
		spdlog::warn("LUA: Expected string for message, got {}", tstr);
	}
	else
	{
		const char* str = lua_tostring(L, -1);
		spdlog::error("{}", str);
	}
	return 0;
}
