#include "kcore/kengine.hpp"
#include "kcore/klbinding.hpp"


#include <spdlog/spdlog.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

namespace karaton::kcore {

GPU_Target* Kengine::get_screen()
{
	return this->screen;
}

lua_State* Kengine::get_lua()
{
	return this->lua;
}

Kengine::Kengine(std::string datadir, int width, int height, bool debuglog)
: datadir(datadir)
{
	/* Initialize spdlog */
	if (debuglog)
		spdlog::set_level(spdlog::level::debug);

	spdlog::info("Initializing karaton engine");


	/* Initialize all of the things here */
	GPU_SetDebugLevel(GPU_DEBUG_LEVEL_MAX);
	this->screen = GPU_Init(width, height, GPU_DEFAULT_INIT_FLAGS);
	spdlog::info("Graphics initialized");

	/* Initialize lua */
	this->lua = luaL_newstate();

	/* Inject karaton functionality into the lua namespace \o/ */
	karaton::scripting::open_karaton(this->lua);

	spdlog::info("Lua initialized");
}

Kengine::~Kengine()
{
	/* Shut down all of the things here */
	GPU_Quit();
	spdlog::info("Freed graphics subsystem");
	lua_close(this->lua);
	spdlog::info("Freed lua subsystem");
}

}
