#include "kcore/klbinding.hpp"
#include "kcore/low/klua.hpp"

#include <vector>
#include <spdlog/spdlog.h>


namespace karaton::scripting {

bool open_kconsole(lua_State* L)
{
	std::vector<luaL_Reg> lua_console_module;
	lua_console_module.push_back({"message", l_message});
	lua_console_module.push_back({"warn", l_warn});
	lua_console_module.push_back({"error", l_error});
	lua_console_module.push_back({"debug", l_debug});
	if (!karaton::kcore::low::register_namespace(L, "karaton", "console", lua_console_module))
	{
		spdlog::error("Failed to register kconsole module");
		return false;
	}
	return true;
}

void open_karaton(lua_State* L)
{
	lua_newtable(L);
	lua_setglobal(L, "karaton");
}


}

