/*
	This module is a sort of simplified wrapper for lua shit to make wazubaba's
	life easier. It might not be the best. It probably is not even well-made.
	But it does aim to improve my will to dabble in this horrifying hydra of a
	language (not lua, cpp)
*/

#include "kcore/low/klua.hpp"
#include <spdlog/spdlog.h>

namespace fs = boost::filesystem;

namespace karaton::kcore::low {

bool register_namespace(lua_State* L, const std::string& root, const std::string& name, const std::vector<luaL_Reg>& funcs)
{
	lua_getglobal(L, root.c_str());
	if (lua_isnil(L, -1))
	{
		spdlog::error("klua: error registering namespace: root {} does not exist", root);
		return false;
	}

	if (!lua_istable(L, -1))
	{
		spdlog::error("klua: error registering namespace: root {} is not a table", root);
		return false;
	}

	lua_newtable(L);
	for (auto &func : funcs)
	{
		lua_pushcfunction(L, func.func);
		lua_setfield(L, -2, func.name);
	}

	lua_setfield(L, -2, name.c_str());

	return true;
}

bool load_file(lua_State* L, fs::path path)
{
	spdlog::debug("Loading luascript from [{}]", path.native());
	int result = luaL_dofile(L, path.c_str());
	if (result != LUA_OK)
	{
		spdlog::error("Failed to load {}", path.native());
		return false;
	}
	spdlog::info("Loaded script [{}]", path.native());
	return true;
}

}

