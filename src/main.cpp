#include <lua.hpp>
#include <spdlog/spdlog.h>
#include <boost/filesystem.hpp>

#include "kcore/klbinding.hpp"
#include "kcore/low/klua.hpp"

#include "kcore/kengine.hpp"

namespace fs = boost::filesystem;

#ifdef DEBUG
	#define ROOT "../"
#else
	#define ROOT "."
#endif



int main(int argc, char** argv)
{
	/* TODO: Add an option to change the root so we don't need to fuck with root
	as a const for debugging purposes*/

	/* Set the current dir to the root directory */
	boost::filesystem::current_path(ROOT);
	spdlog::debug("Current path: {}", fs::current_path().native());

  spdlog::info("Initializing lua");
	lua_State* L = luaL_newstate();
	spdlog::info("Initializing lua stdlib");
	luaL_openlibs(L);

	spdlog::info("Registering engine functions to lua");
	karaton::scripting::open_karaton(L);
	karaton::scripting::open_kconsole(L);


	if (!karaton::kcore::low::load_file(L, fs::path("data/scripts/main.lua")))
		goto shutdown;

	lua_getglobal(L, "karaton");
	if (!lua_istable(L, -1))
	{
		spdlog::error("karaton namespace is not a valid table!");
		goto shutdown;
	}

	lua_getfield(L, -1, "init");

  if (lua_isfunction(L, -1))
  {
		spdlog::info("Got init function!");
		int result;
		result = lua_pcall(L, 0, 0, 0);
		if (result != LUA_OK)
		{
			spdlog::error("Failed to run init function");
			goto shutdown;
		}
  }
  else
		spdlog::warn("No init function found!");

shutdown:
  lua_close(L);
  return 0;
}
