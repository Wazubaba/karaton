/*
	This module is a sort of simplified wrapper for lua shit to make wazubaba's
	life easier. It might not be the best. It probably is not even well-made.
	But it does aim to improve my will to dabble in this horrifying hydra of a
	language (not lua, cpp)
*/
#pragma once
#include <lua.hpp>
#include <string>
#include <vector>
#include <boost/filesystem.hpp>
namespace karaton::kcore::low {

bool register_namespace(lua_State* L, const std::string& root, const std::string& name, const std::vector<luaL_Reg>& funcs);

/* Load the target script into the provided state */
bool load_file(lua_State* L, boost::filesystem::path path);


}

