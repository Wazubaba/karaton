#pragma once

#include "lua/klconsole.hpp"

namespace karaton::scripting
{
	bool open_kconsole(lua_State* L);
	void open_karaton(lua_State* L);
}
