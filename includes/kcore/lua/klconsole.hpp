#pragma once

#include <lua.hpp>

int l_debug(lua_State* L);
int l_message(lua_State* L);
int l_warn(lua_State* L);
int l_error(lua_State* L);
