#pragma once

/**
	This module handles low-level initialization of various sub-components for the
	engine as well as initializing the base graphics shit for sdl_gpu
**/

#include <SDL2/SDL.h>
#include <SDL2/SDL_gpu.h>
#include <lua.hpp>
#include <string>



namespace karaton::kcore {

class Kengine
{
public:
	Kengine(std::string datadir, int width, int height, bool debuglog);
	~Kengine();

	GPU_Target* get_screen();
	lua_State* get_lua();

private:
	GPU_Target* screen;
	lua_State* lua;
	std::string datadir;
};

}
