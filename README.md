# K A R A T O N

#### Dependencies
* [luajit](https://luajit.org/) (2.1)
* [spdlog](https://github.com/gabime/spdlog) (bundled as submodule)
* [boost-filesystem](https://www.boost.org/doc/libs/1_39_0/libs/filesystem/doc/index.htm) (1.69.0)

